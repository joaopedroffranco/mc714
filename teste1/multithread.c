#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <string.h>

int pot2_20 = 1024*1024;

/* Função para gerar um numero aleatório */
long random_number(int min, int max){
      int result = 0, low_num = 0, hi_num = 0;
      if(min<max){
          low_num=min;
          hi_num=max+1;
      } else {
          low_num=max+1;
          hi_num=min;
      }

      srand(time(NULL));
      result = (rand()%(hi_num-low_num))+low_num;

      return result;
 }

/* Função que será executada por cada thread. Passando o vetor v, tamanho size do mesmo, número total k de threads utilizadas e um iteração i atual, ele popula sua parte do vetor e calcula a soma dessa parte */
void *fthread(void *arg){
 	long **parms = (long**) arg;

 	// parametros
 	long *v = parms[0];
 	long size = *parms[1];
 	long k = *parms[2];
 	long i = *parms[3];

 	// interval: o range que essa thread populará do vetor
 	long interval = size/k;

 	// start: qual elemento do vetor inicial ele começa a popular
 	long start = i*interval;

	int c;
	long sum = 0;
	for (c = start; c < size && c < start + interval; c++) {
		v[c] = random_number(0, 1001);
		sum += v[c];
	}

	free(parms);

	// A função retorna um *void
	long *pointerSum = (long*) malloc(sizeof(long));
	*pointerSum = sum;

	// retornamos a soma parcial como um *void
	return pointerSum;
}

/* MAIN: Versão multithreads */
int main(int x, char* a[]){
	long i, j;

	// n: numero n passado pelo primeiro parametro
	long n = atoi(a[1]);

	// k: numero k passado pelo segundo parametro de threads
	long k = atoi(a[2]);

	// size: tamanho do vetor de numeros
	long size = n*pot2_20;

	// apontador para size
	long *sizePointer = (long*) malloc(sizeof(long));
	*sizePointer = size;

	// apontator para k
	long *kPointer = (long*) malloc(sizeof(long));
	*kPointer = k;

	// vetor de threads
	pthread_t *pthreads = (pthread_t*) malloc(k*sizeof(pthread_t));

	// vetor de numeros
	long *numbers = (long*) malloc(size*sizeof(long));

	// para cada thread, passamos os parametros necessários e criamos a thread de fato.
	for (i = 0; i < k; i++){
		long **parms = (long**)malloc(4*sizeof(long*));
		parms[0] = numbers; 
		parms[1] = sizePointer;
		parms[2] = kPointer;

		long *iPointer = (long*) malloc(sizeof(long));
		*iPointer = i;
		parms[3] = iPointer;
		pthread_create(&(pthreads[i]), NULL, fthread, parms);
	}

	long sum = 0;

	// após criar as threads, executamos elas. Como cada thread retorna uma soma parcial (como *void), fazemos um cast para long e somamos as parciais.
	for (i = 0; i < k; i++){
		void *returnSum;
		pthread_join(pthreads[i], &returnSum);
		long *returnSumLong = (long*) returnSum;
		sum += *returnSumLong;

		free(returnSum);
	}

	free(numbers);	

	// Ao final, printamos o resultado.
	printf("Média = %ld\n", sum/size);
}