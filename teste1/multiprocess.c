#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <time.h>

int pot2_20 = 1024*1024;

/* Struct do objeto que será mapeado para memória compartilhada, no caso, contem o vetor de numeros e a soma total dos elementos */
typedef struct shmObject{
	long *numbers;
	long sum;
} shmObject;

/* Função para gerar um numero aleatório */
long random_number(int min, int max){
      int result = 0, low_num = 0, hi_num = 0;
      if(min<max){
          low_num=min;
          hi_num=max+1;
      } else {
          low_num=max+1;
          hi_num=min;
      }

      srand(time(NULL));
      result = (rand()%(hi_num-low_num))+low_num;

      return result;
 }

/* MAIN: Versão multiprocessos */
int main(int x, char* a[]){
	// abrimos um objeto de memória compartilhada, com caracteristicas de read-write.
	int fd = shm_open("multiprocess area", O_RDWR | O_CREAT, 0666);

	long i, j;

	// n: numero n passado por primeiro parametro
	long n = atoi(a[1]);

	// k: numero k passado por segundo parametro de processos
	long k = atoi(a[2]);

	// size: tamanho do vetor de numeros
	long size = n*pot2_20;

	// apontador de size
	long *sizePointer = (long*) malloc(sizeof(long));
	*sizePointer = size;

	// apontador de k
	long *kPointer = (long*) malloc(sizeof(long));
	*kPointer = k;

	// definindo o tamanho do objeto de SHM, no caso, será tamanho de uma estrutura shmObject criado
	ftruncate(fd, sizeof(shmObject));

	// definindo o mapa do objeto criado, com permissão de ler, escrever e compartilhar
	void *ptr = mmap(NULL, sizeof(shmObject), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	// com mapa criado, fazemos um cast para o tipo shmObject e inicializamos os atributos.
	shmObject *shmO = (shmObject*) ptr;
	shmO->numbers = (long*) malloc(size*sizeof(long));
	shmO->sum = 0;

	// criamos um processo
	pid_t pid;

	// para cada processo que será criado, fazemos um fork
	for (i = 0; i < k; i++){
		pid = fork();

		// se for filho, ele populará sua parte do vetor, com a mesma lógica dos threads.
		if (pid == 0){
			long interval = size/k;
			long start = i*interval;
			for (j = start; j < size && j < start + interval; j++) {
				shmO->numbers[j] = random_number(0, 1001);
				shmO->sum += shmO->numbers[j];
			}
			exit(0);
		} 
	}

	// esperamos todos os processos filhos criados terminarem de popular suas partes do vetor e calcular suas somas parciais
	int status;
	while ((pid = wait(&status)) != -1) {
	}

	// ao final, temos a soma total e printamos o resultado
	printf("Média = %ld\n", shmO->sum/size);

	// fechamos o objeto de memoria compartilhada
	munmap(ptr, size);
	shm_unlink("multiprocess area");
}