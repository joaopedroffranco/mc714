MC714 - Teste 1
=============
João Pedro Fabiano Franco
146641


Foi feito nesse teste uma aplicação com a seguinte idéia geral: 
> Um programa que popule, com números aleatórios, um vetor relativamente grande e que some todos os elementos do mesmo, para calcular a média dos valores.

Foi usado três versões:
> * Sequencial;
> * Multithread;
> * Multiprocesso.

Sequencial
-------------
Basicamente, alocamos um vetor de tamanho N*2ˆ20, e itera sobre ele, gerando números aleatórios para cada posição i, e ao mesmo tempo, incrementa uma soma parcial. Ao final de iteração, divimos pelo tamanho do vetor para obter a média.

Os calculos são feitos de maneira "linear". Logo, como vetor é relativemente grande, foi gasto um tempo grande para definir o resultado. Em torno de 15s para N = 64. Quanto maior o N, maior o tempo. 

|  N  | Tempo |
|:---:|-------|
|  1  |   1s   |
|  10 |   3s   |
| 64  |   15s  |
| 100 |   23s  |
| 200 |   47s  |
Tabela: relação N x Tempo de execução

Multithread
-------------
Sobre o mesmo vetor, temos k thread. Cada thread criada fica com tamanho/k do vetor para gerar números aleatórios e calcular sua soma parcial. Ao final, tem-se uma soma total, a qual é dividio pelo tamanho, obtendo-se a média.

Nessa versão, foi usado threads para dividir o trabalho de popular o vetor e calcular as somas parciais de cada parte, para depois termos a média. Dividimos igualmente o vetor entre os K threads. Já é de se esperar que o tempo de execução diminua, e de fato, acontece. 

Para os mesmos N = 64, executa-se para K = 2 em 8s, já foi melhor que o sequencial. Para K = 4 e K = 8, consegue-se diminuir para 5s. 

Pode-se observar que aumentando o número de threads não significa diminuir o tempo. Com K = 100 e depois K = 1000, não passa de 4s. 

|  K  | Tempo |
|:---:|-------|
|  2   |   8s   |
|  4   |   5s   |
|  8   |   5s   |
|  100 |   4s   |
| 1000 |   4s   |
Tabela: relação K x Tempo de execução para N = 64

Multiprocesso
-------------
Nessa versão, substitui-se as threads pelos processos, usando memória compartilhada, no caso, o vetor de número, e as somas parciais, mantendo-se a mesma lógica: dividir e depois conquistas. Os resultados iniciais são relativamente melhores que os da versão multithreads. 

|  K  | Tempo |
|:---:|-------|
|  2   |   7s   |
|  4   |   5s   |
|  8   |   5s   |
|  100 |   3s   |
| 1000 |   3s   |
Tabela: relação K x Tempo de execução para N = 64





