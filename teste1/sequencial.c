#include <stdio.h>
#include <stdlib.h>
#include <time.h>

long pot2_20 = 1024*1024;

/* Função para gerar um numero aleatório */
long random_number(int min, int max){
      int result = 0, low_num = 0, hi_num = 0;
      if(min<max){
          low_num=min;
          hi_num=max+1;
      } else {
          low_num=max+1;
          hi_num=min;
      }

      srand(time(NULL));
      result = (rand()%(hi_num-low_num))+low_num;

      return result;
 }

/* MAIN: Versão sequencial */
int main(int x, char* a[]){
	// contador i
	int i; 

	// n: numero n passado pelo primeiro parametro
	long n = atoi(a[1]);

	// size: tamanho total do vetor de numeros
	long size = n*pot2_20;

	// alocando o vetor numbers
	long* numbers = (long*) malloc(size*sizeof(long));
	long sum = 0;
	for (i = 0; i < size; i++){
		long number = random_number(0, 1001);
		numbers[i] = number;
		sum += numbers[i];
	}

	// printa os resultados
	printf("Média = %ld\n", sum/size);
}