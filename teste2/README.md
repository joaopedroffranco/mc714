MC714 - Teste 2
=============
João Pedro Fabiano Franco
146641


Foi feito nesse teste uma aplicação com estratégia de disseminação de informação entre processos por gossiping usando interfaces de comunicação: 
> Considere uma rede de processos que podem se conectar via sockets. Cada um de N processos nesta rede deve aceitar conexões para receber novas informação, e conectar-se a outros processos para propagar informação. Um único processo inicia a disseminação. Quando em posse de informação nova, cada processo A se conecta com outro processo B escolhido aleatoriamente. Se B já possuía a informação a ser disseminada, A para o processo de disseminação com probabilidade 1/k. Se B não possuía a informação, A repete o processo com outro B escolhido aleatoriamente.

Foi usado implementação de sockets tradicionais TCP/IP, na linguagem Python.

Lógica de implementação
---------------
Foi feito uma implementação Main, em python, que dado um N e um K, ele cria N processos. Cada processo, com exceção do primeiro, tem seu server inicializado com uma porta aleatória, após isso, o primeiro processo inicia a disseminação uma única vez.

Ao conectar-se com um processo, ele envia a informação, e recebe como resposta success ou fail. Caso seja fail, ele morre em uma probabilidade 1/K. Ao tentar se conectar com um processo que já morreu, ele também morre na mesma probabilidade. No processo que recebeu a informação, caso seja novidade, inicia-se uma thread para começar a fofocar.

E a disseminação continua até ninguem mais fofocar ou der timeout de 60 segundos, definido previamente. Após isso, é calculado as estatísticas para responder as perguntas do teste2, baseado no número de tentativas, sucessos e quantidade de processos que participaram na disseminação de fato.
Durante a execução da disseminação, é printado as etapas, quem está fofocando quem, quem morreu, e ao final, número de tentativas e parcela de sucesso de cada processo.

Obs: para o hardware em questão, consegui um N máximo de 200 processos.


Análise dos resultados
---------------
Para analisar os resultados, foi variado N e K:
> * N = 10 e K = 2;
> * N = 100 e K = 2;
> * N = 200 e K = 2;
> * N = 200 e K = 4;
> * N = 200 e K = 8;

| Entrada |  Tentativas  | Sucesso | Processos contaminados | Tempo |
|---------|--------------|---------|------------------------|-------|
|  N = 10 e K = 2   |   21   |  38%  |   8/10   |   12s   |
|  N = 100 e K = 2  |   205   |  45%    |   81/100   |   17s   |
|  N = 200 e K = 2  |   403   |   46%   |  159/200    |   18s   | 
|  N = 200 e K = 4  |   688   |   29%   |  188/200    |   30s   |
|  N = 200 e K = 8  |   1070   |  19%    |  196/200    |  52s   |

Analisando as três primeiras entradas, variando apenas o N, percebe-se o aumento do número de tentativas, uma vez que quando mais processos participandos, mais tentativas vão ter. Como a probabilidade não muda, a parcela de sucesso permaneceu praticamente a mesma, assim como número de processos contaminados manteve a mesma proporção. O tempo se manteve também.

Agora analisando a variação da probabilidade, vemos que quanto menor a chance de morrer (maior o K), maior o número de tentativas, uma vez que o processo não morrerá tão fácil, então seguirá tentando. Porém a parcela de sucesso diminui, pois muitos processos vão receber a informação, mas não vão morrer, obtendo tentativas falhas. E como ficou mais dificil morrer, maior a chance de contaminar mais processos, como podemos ver na tabela. O tempo cresceu muito, pois ele passa mais tempo tentando procurar alguém que não recebeu a informação, e em paralelo, ele se mantem mais tempo vivo fazendo isso. 