from socket import *
import time
import random
import string
import sys
import os
import numpy
from multiprocessing import Process, Queue
from subprocess import Popen
from threading import Thread
try:    
    import thread 
except ImportError:
    import _thread as thread

BUFF = 1024

minPort = 50000
maxPort = 65000
localhost = 'localhost'

news = 'informacao'.encode()
fail = 'já recebi'.encode()
success = 'novidade'.encode()

usedPorts = []
processes = []

## PRIMEIRO PROCESSO FOFOCA
def gossipfirst(p_port_gossip, queue_gossip_first):
	randomNumber = random.randint(0, N-1) # escolha-se uma porta aleatória
	port = usedPorts[randomNumber]
	while port == p_port_gossip: # enquanto nao for uma porta que não seja o do proprio processo, escolhe outra
		randomNumber = random.randint(0, N-1) 
		port = usedPorts[randomNumber]

	ADDRClient = (localhost, port) # tupla de endereco do client
	clientSocket = socket(AF_INET, SOCK_STREAM) # criando socket do client

	pT, pS, pR, pA = queue_gossip_first.get() # incrementando numero de tentativas desse processo
	pT[0] += 1
	queue_gossip_first.put((pT, pS, pR, pA))

	try: 
		clientSocket.connect(ADDRClient) # tenta conectar com processo escolhido
		print('Processo', p_port_gossip, 'fofocando com', port)
		clientSocket.send(news) # se conectou, manda a informacao

		anwser = clientSocket.recv(BUFF) # recebe a resposta

		pT, pS, pR, pA = queue_gossip_first.get() # como é primeiro processo, ele vai conseguir disseminar, entao incrementa o numero de sucesso
		pS[0] += 1
		queue_gossip_first.put((pT, pS, pR, pA))

		clientSocket.close() # fecha socket
	except:
		pT, pS, pR, pA = queue_gossip_first.get() # teoricamente, nao caira aqui. Apenas por problemas externos
		pA[0] = 0
		queue_gossip_first.put((pT, pS, pR, pA))

## PROCESSO FOFOCA 
def gossip(index_gossip, p_port_gossip, queue_gossip):
	pT, pS, pR, pA = queue_gossip.get() # atualiza status do para 2 (fofocando)
	pA[index_gossip] = 2
	queue_gossip.put((pT, pS, pR, pA))

	while 1:
		time.sleep(1)

		randomNumber = random.randint(0, N-1) 
		port = usedPorts[randomNumber]
		while port == p_port_gossip: # enquanto nao for uma porta que não seja o do proprio processo, escolhe outra
			randomNumber = random.randint(0, N-1) 
			port = usedPorts[randomNumber]

		pT, pS, pR, pA = queue_gossip.get()
		queue_gossip.put((pT, pS, pR, pA))
		if pA[randomNumber] == 0: # se o processo destino ja morreu,
			if (random.randint(1, K) == K): # com probabilidade 1/K, esse processo morre tambem
				print("	Processo", p_port_gossip, "morreu ao tentar conectar com", port, "que ja morreu")

				pT, pS, pR, pA = queue_gossip.get() # se morrer, atualiza status = 0 (morto)
				pA[index_gossip] = 0
				queue_gossip.put((pT, pS, pR, pA))

				break

		ADDRClient = (localhost, port) # tupla de endereco do client
		clientSocket = socket(AF_INET, SOCK_STREAM) # criando socket de client

		try: 
			clientSocket.connect(ADDRClient) # tenta conectar com processo escolhido
			print('Processo', p_port_gossip, 'fofocando com', port)

			pT, pS, pR, pA = queue_gossip.get() # incrementando numero de tentativas desse processo
			pT[index_gossip] += 1
			queue_gossip.put((pT, pS, pR, pA))

			clientSocket.send(news) # se conectou, manda a informacao
			anwser = clientSocket.recv(BUFF) # recebe a resposta do server destino
			if anwser == fail: # se nao foi novidade
				if (random.randint(1, K) == K): # com probabilidade 1/K, esse processo morre
					print("	Processo", p_port_gossip, "morreu por mandar informação para", port, "que não é novidade")

					pT, pS, pR, pA = queue_gossip.get() # se morrer, atualiza status = 0 (morto)
					pA[index_gossip] = 0
					queue_gossip.put((pT, pS, pR, pA))

					break
			else:
				pT, pS, pR, pA = queue_gossip.get() # se é novidade, incrementa o numero de sucesso desse processo
				pS[index_gossip] += 1 
				queue_gossip.put((pT, pS, pR, pA))

			clientSocket.close() # fecha socket de client
		except:
			nothing = ""

	pT, pS, pR, pA = queue_gossip.get()
	queue_gossip.put((pT, pS, pR, pA))
	end = True
	for a in pA: # caso não tenha nenhum processo fofocando (status 2), termina a disseminação
		if a == 2:
			end = False
			break

	if end:
		print(" ") # printa o tempo total de disseminação e espera dar timeout
		print("Fofoca durou", time.time() - now - 5, "segundos. Esperando dar timeout...")

	sys.exit(0) # mata processo

def process(index_process, p_port_process, queue_process):
	if index_process == 0: # se for primeiro processo
		mydata = news # ja atualiza o mydata com informacao ja recebida, ja que comeca com ele

		pT, pS, pR, pA = queue_process.get() # atualiza o processo para que ja recebeu a informacao
		pR[index_process] = 1
		queue_process.put((pT, pS, pR, pA))
	else: 
		mydata = " " # inicializa mydata vazio

		ADDRServer = (localhost, p_port_process) # tupla de endereco do server
		serverSocker = socket(AF_INET, SOCK_STREAM) # criando socket do server
		serverSocker.bind(ADDRServer) # sobe o socket
		serverSocker.listen(5)

		while 1:
			time.sleep(1)

			pT, pS, pR, pA = queue_process.get() 
			queue_process.put((pT, pS, pR, pA))
			if pA[index_process] == 0: # se status for 0 (morto), derruba o server
				break

			connSocket, client = serverSocker.accept() # espera um client conectar
			data = connSocket.recv(BUFF) # cria socket de conexao e recebe a informacao
			if data == mydata:  # se nao é novidade
				connSocket.send(fail) # responde o client falando que nao é novidade
			else: # se é novidade
				pT, pS, pR, pA = queue_process.get() # atualiza o processo para que ja recebeu a informacao
				pR[index_process] = 1
				queue_process.put((pT, pS, pR, pA))

				mydata = news # atualiza mydata
				connSocket.send(success) # responde client com sucesso
				try: # tenta criar thread para comecar a fofocar a novidade
				   thread.start_new_thread(gossip, (index_process, p_port_process, queue_process))
				except:
				   print("Error: unable to start thread")

			connSocket.close() # fecha socket conexao

		serverSocker.close() # fecha socket do server
		sys.exit(0) # mata processo

if __name__ == '__main__':
	now = time.time()

	N = int(sys.argv[1]) # recebe N por parametro
	K = int(sys.argv[2]) # recebe K por parametro

	processTry = numpy.empty(N); processTry.fill(0) # inicializa vetor de que guarda numero de tentativas de cada processo
	processSuccess = numpy.empty(N); processSuccess.fill(0) # inicializa vetor de que guarda numero de sucesso de cada processo
	processRecv = numpy.empty(N); processRecv.fill(0) # inicializa vetor de que guarda se cada processo recebeu a informacao
	processAlive = numpy.empty(N); processAlive.fill(1) # inicializa vetor de que guarda status de cada processo (0: morto, 1: vivo, 2: fofocando)

	q = Queue() # criando queue, para trocar informacoes
	q.put((processTry, processSuccess, processRecv, processAlive)) 

	print("---- Inicializando os processos ----")
	for i in range(N): # inicializando os N processos
		porti = random.randint(minPort, maxPort) # escolhe uma porta aleatoria
		while porti in usedPorts: # enquanto nao foi usado a porta ainda
			porti = random.randint(minPort, maxPort)

		p = Process(target=process, args=(i, porti, q)) # cria processo, chamando a funcao process
		processes += [p] # vetor de processos
		usedPorts += [porti] # vetor de portas usadas

	for process in processes: # start todos os processos
		process.start()

	time.sleep(5) # espera 5s para inicializar todos os servers
	print("Portas sendo usadas:", usedPorts)
	print(" ")
	print("---- Começo da fofoca ----")
	gossipfirst(usedPorts[0], q) # primeiro processo comeca a fofoca

	time.sleep(60) # timeout de 60s

	for process in processes: # forca terminar todos os processo
		process.terminate()

	print("---- Fim da fofoca ----")
	print(" ")

	processTry, processSuccess, processRecv, processAlive = q.get() # recebe os vetores depois do processo de disseminacao
	countRecv = 1 # contador de processos que receberam a informacao 
	sumTry = 0 # contador de tentativas
	sumSucess = 0 # contador de sucessos
	for c in range(N):
		sumTry += processTry[c]
		sumSucess += processSuccess[c]

		if c == 0:
			print("Processo", usedPorts[c],"é o primeiro, ele apenas começou a fofoca")
		else :
			if processTry[c] == 0:
				print("Processo", usedPorts[c], "não recebeu e não chegou a fofocar")
			else :
				if processTry[c] == 1: 
					print("Processo", usedPorts[c], "tentou", processTry[c], "vez, com sucesso de", (processSuccess[c]/processTry[c])*100,"%")
				else:
					print("Processo", usedPorts[c], "tentou", processTry[c], "vezes, com sucesso de", (processSuccess[c]/processTry[c])*100,"%")
				if processRecv[c] == 1:
					countRecv += 1

	print(" ")
	print("De uma maneira geral, teve", sumTry, "tentativas, com sucesso de", (sumSucess/sumTry)*100,"%")
	print("De", N, "processos,", countRecv, "receberam a informação")

	sys.exit(0)





	




